#!/usr/bin/env python


import rospy
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseFeedback
import actionlib
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, Point, PoseWithCovarianceStamped, Quaternion, PoseStamped
from nav_msgs.srv import GetPlan
from nav_msgs.msg import Odometry

def callback_pose(data):
        global start_x
        global start_y
        start_x = data.pose.pose.position.x
        start_y = data.pose.pose.position.y
        with open('test.txt', 'a+') as f:
             f.write(str(start_x)+"  "+str(start_y)+"\n")


class GoToPose():
    def __init__(self):
        self.goal_sent = False
        # What to do if shut down (e.g. Ctrl-C or failure)
        rospy.on_shutdown(self.shutdown)
        #rospy.Subscriber('Odom', Odometry, odometryCb)
        # Tell the action client that we want to spin a thread by default
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        rospy.loginfo("Wait for the action server to come up")

        # Allow up to 5 seconds for the action server to come up
        self.move_base.wait_for_server(rospy.Duration(5))
   
    def goto(self, pos, quat):

        # Send a goal
        self.goal_sent = True

        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose = Pose(Point(pos['x'], pos['y'], 0.000),
        Quaternion(quat['r1'], quat['r2'], quat['r3'], quat['r4']))

    # Start moving
        self.move_base.send_goal(goal)
        
    # Allow TurtleBot up to 60 seconds to complete task
        success = self.move_base.wait_for_result(rospy.Duration(60)) 

        state = self.move_base.get_state()
        result = False

        if success and state == GoalStatus.SUCCEEDED:
            result = True
        else:
            self.move_base.cancel_goal()

        self.goal_sent = False
        return result

    def shutdown(self):
        if self.goal_sent:
            self.move_base.cancel_goal()
        rospy.loginfo("Stop")
        rospy.sleep(1)


if __name__ == '__main__':
    start_x=1.57
    start_y=-3.59
    try:
        rospy.init_node('nav_test', anonymous=False)
        rospy.Subscriber ('/amcl_pose',PoseWithCovarianceStamped, callback_pose)
        
       
        #Plan go to position 1 
        #Wait for the availability of this service
        rospy.wait_for_service('move_base/make_plan')
        #Get a proxy to execute the service
        make_plan = rospy.ServiceProxy('move_base/make_plan', GetPlan)

        start = PoseStamped()
        goal = PoseStamped()
        start.header.frame_id = "map"
        goal.header.frame_id = "map"
        tolerance = 0.0
        
        start.pose.position.x= start_x 
        start.pose.position.y= start_y
        goal.pose.position.x = -1.77
        goal.pose.position.y = -4.52

       
        plan_response = make_plan(start = start, goal = goal, tolerance = tolerance)

        poses = plan_response.plan.poses
        with open('plan.txt', 'a+') as f:
            for index in range(1, len(poses)):
                f.write(str(poses[index].pose.position.x)+"  "+str(poses[index].pose.position.y)+"\n")

        #print (poses)
     

        # Go to position 1
        rospy.sleep(5)
        rospy.init_node('nav_test', anonymous=False)
        navigator = GoToPose()
        position_1 = {'x': -1.77, 'y' : -4.52}
        quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : 0.000, 'r4' : 1.000}
        #rospy.loginfo("Go to (%s, %s) pose", position['x'], position['y'])
        success_1 = navigator.goto(position_1, quaternion)
        
        # we need to pass status to Ar
        if success_1:
            rospy.loginfo("Reached the desired pose")
        else:
            rospy.loginfo("Failed to reach the desired pose")

        #Plan go to position 2 
         #Wait for the availability of this service
        rospy.wait_for_service('move_base/make_plan')
        make_plan = rospy.ServiceProxy('move_base/make_plan', GetPlan)
        
        start.pose.position.x= start_x 
        start.pose.position.y= start_y
        #start.pose.position.x = -1.77
        #start.pose.position.y = -4.52
        goal.pose.position.x = -1.6
        goal.pose.position.y = -0.26

        plan_response = make_plan(start = start, goal = goal, tolerance = tolerance)

        poses2 = plan_response.plan.poses
        with open('plan.txt', 'a+') as f:
            for index in range(1, len(poses2)):
                f.write(str(poses2[index].pose.position.x)+"  "+str(poses2[index].pose.position.y)+"\n")

        #print (poses)
        
        rospy.sleep(5)
        rospy.init_node('nav_test', anonymous=False)
        navigator = GoToPose()
        position_2 = {'x': -1.6, 'y' : -0.26}
        quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : 0.000, 'r4' : 1.000}
        #rospy.loginfo("Go to (%s, %s) pose", position['x'], position['y'])
        success_2 = navigator.goto(position_2, quaternion)

        if success_2:
            rospy.loginfo("Reached the desired pose")
        else:
            rospy.loginfo("Failed to reach the desired pose")
       

       
        
       
       
    except rospy.ROSInterruptException:
       rospy.loginfo("Ctrl-C caught. Quitting")
