#!/usr/bin/env python 

from subprocess import call
import time

def main():
	# send the path file to server only once
	call(['scp','plan.txt','server:/home/zhanwang/Temp'])
    while True:
    	# send the current location to server about every 1/3 seconds
        call(['scp','data.txt', 'server:/home/zhanwang/Temp'])
        time.sleep(0.2)
if __name__ == '__main__':
    main()
