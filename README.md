# README #

Final Project for CIS693 

### What is this repository for? ###

* Augment Reality application on robot
* 0.0.1


### Project Demo ###

https://www.youtube.com/watch?v=3MrmlRhLawY

### Contribution guidelines ###
* Zhan Wang: Use artool to draw the trajactory of robot 
* Xiaodan Liu: Draw the map of robot. Wrote a node to return two coordinating position of starting position and tareget position of the robot relative to the marker
* Suhua Wei: Figure out the coordinating tranfermation between the artool and robot mapping .


### Who do I talk to? ###
* s.wei@vikes.csuohio.edu